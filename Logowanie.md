# Uzyskiwanie wymaganych informacji

Do uzyskania certyfikatu PFX potrzebnego do podpisywania żądań potrzebne są następujące informacje:

- Token
- Symbol - potrzebny również przy każdym żądaniu
- PIN

W celu uzyskania potrzebnych do logowania informacji użytkownik musi odwiedzić stronę dziennika UONET+, przejść do sekcji Uczeń, następnie Dostęp mobilny i kliknąć przycisk `Zarejestruj urządzenie mobilne`.

![Ekran rejestracji urządzenia w systemie UONET+ firmy Vulcan](/img/rejestracja_urzadzenia_uonetplus.png)

## Uzyskiwanie odpowiedniego adresu REST API

Żądanie `GET` na adres

```
http://komponenty.vulcan.net.pl/UonetPlusMobile/RoutingRules.txt
```

zwraca w odpowiedzi możliwe pierwsze 3 znaki tokenu podanego przez użytkownika i odpowiednie dla nich adresy REST API, na które należy kierować żądania.

Przykładowa odpowiedź:

```
3S1,https://lekcjaplus.vulcan.net.pl
TA1,https://uonetplus-komunikacja.umt.tarnow.pl
OP1,https://uonetplus-komunikacja.eszkola.opolskie.pl
RZ1,https://uonetplus-komunikacja.resman.pl
GD1,https://uonetplus-komunikacja.edu.gdansk.pl
P03,https://efeb-komunikacja-pro-efebmobile.pro.vulcan.pl
P01,http://efeb-komunikacja.pro-hudson.win.vulcan.pl
P02,http://efeb-komunikacja.pro-hudsonrc.win.vulcan.pl
P90,http://efeb-komunikacja-pro-mwujakowska.neo.win.vulcan.pl
```

Zgodnie z powyższą odpowiedzią, jeśli token podany przez użytkownika to np. `OP12137DD`, to odpowiednim dla żądań adresem będzie `https://uonetplus-komunikacja.eszkola.opolskie.pl`.

## Uzyskiwanie certyfikatu PFX

W celu uzyskania certyfikatu PFX potrzebnego do podpisywania żądań, należy wysłać żądanie `POST` na adres:

```
${odpowiedni adres REST API}/${symbol}/mobile-api/Uczen.v3.UczenStart/Certyfikat
```

Wśród nagłówków żądania powinny znaleźć się następujące:

```
RequestMobileType: RegisterDevice
User-Agent: MobileUserAgent
Content-Type: application/json
```

W treści `BODY`powinny znajdować się następujące informacje:

```json
{
    "PIN": ${pin},
    "TokenKey": "${token}",
    "AppVersion": "18.4.1.388",
    "DeviceId": "${random UUID v4}",
    "DeviceName": "${producent urządzenia}#${model urządzenia}",
    "DeviceNameUser": "",
    "DeviceDescription": "",
    "DeviceSystemType": "Android",
    "DeviceSystemVersion": "6.0.1",
    "RemoteMobileTimeKey": ${obecny czas w formacie Unix time},
    "TimeKey": ${obecny czas w formacie Unix time - 1},
    "RequestId": "${random UUID v4}",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```



Przykładowa odpowiedź:

```json
{
    "IsError": false,
    "IsMessageForUser": false,
    "Message": null,
    "TokenKey": null,
    "TokenStatus": "CertGenerated",
    "TokenCert": {
        "CertyfikatKlucz": "AD549D625A292010D94ACB6A1BB1C91B380E7383",
        "CertyfikatKluczSformatowanyTekst": "AD-54-9D-62-5A-29-20-10-D9-4A-CB-6A-1B-B1-C9-1B-38-0E-73-83",
        "CertyfikatDataUtworzenia": 1524675211,
        "CertyfikatDataUtworzeniaSformatowanyTekst": "2018.04.25 godz. 18:53:31",
        "CertyfikatPfx": "MIACAQMwgAYJKoZIhvcNAQcBoIAkgASCA+gwgDCABgkqhkiG9w0BBwGggCSABIIDFTCCAxEwggMNBgsqhkiG9w0BDAoBAqCCArIwggKuMCgGCiqGSIb3DQEMAQMwGgQUjfDpFSm6zLBmPfsddKgtXDwQ7WUCAgQABIICgE5ITQQ14XVLXBdAdgLnuQJNk+8Qbwv+wNPvwsuE7NPgDHgYtNJ0/65mD/InNAxzHslIoudgBaRBqmmqcfUircEICuGo4XzqSbTpq48dV52Z6zSqpd1ktGH8V6HNyGi2N/zSHQhqotatNNUO8Aq3bj6x+lytODM7QibqxdYy4xv2o26ufHvQg+16aabQscyYj9usJSRXQlwxTTyyIGV00LSYCDxe+pxe63tkZNn+sDHKuYRqGBLCdBu+b6vr8ejah4qTXes93L14D2ccF5dtZZbnw4l2bRfv5zBglSEuyeSQrh4BabTPSAEeiqe4VWPgzaRMXe/eHBOuwbwn9duiSQYI+3fyd/ynRoLYB1T5HSVtrw0gPAiHT9k7P1J8CGg9WvQ9XDVL8vMtAfvkAjPBK7i6Fu0fCShNfuwHVLc5DIFa8CvEJvaI/y+759YXFbTccFqVSU41cvXVTvnY63iOOzvK65pPYl5RsEq5MCLV+YWCcAfv3hBTqGG6ppHckzy39oSlQ9rePTRmlk36fVAD4jXOkoymk8rVyB7vd+HBdJ/eyaEoGGtkdeapyp++LRcO48dgzcB9Us2GXZxNLt8sN/tyb9giAKqHMnzR+2o3FWPIL7/BW2ICetvLhNejOPuOyp3n+yzd1fq+kf/Nc6XFg3mJeEpO34ddKZDXU3jFgSwgMjsbMOGgPLYMRsSV0XTwAyPqCd3As+GF8N9as9PG6m6h8JUAPpGx4PoyJBptxEq6LS7mODn1DWeN7/eHwYWaG5zOvJpCMfGhIIWlarulSZIN7qy5qlaFTRQ9Z3HixWYvmNNBEW2lZPEE++FI35mr/VnONWPTgzY19vVJgfypjM0xSDAhBgkqhkiG9w0BCRQxFB4SAEwAbwBnAGkAbgBDAGUAcgB0MCMGCSqGSIb3DQEJFTEWBBS5GikfvCDBSM0TL68ZNFUg1S0bgwAAAAAAADCABgkqhkiG9w0BBwaggDCAAgEAMIAGCSqGSIb3DQEHATAoBgoqhkiG9w0BDAEGMBoEFEPb03MmaNY3c9yIzLvDf8WuVaYjAgIEAKCABIICqIEQZuDvbZcnvED0x+zhNc7GvhgZ1HDbIWOg7Z3tjrYbK1IiruTK2UiMerEy/S0sHLPjI5lvboI/BNpbnVsKONSFAjG6VlEbIJlj6L52uOXCv/xlUnu6z6dMclbwmiPnzzsQCp/nBIICTxtF1gu9KWVC8fLDGfeiWBcRDe2jKNYzp8mB9K4htt+O0S/FgX6si75BOwZMQrJGWjo2HrXXwRE8BDQOLEPiqSklD+YJaTDaC3oU5OMxpufHFNwJ0A0Rv+uj3zeD1eKTn5iE9SEaSjzslqbpZtvgNKhPKJ4jhhpA9O9LvYmqQWxVh6tHIr5I5Fkc1mAjIug4RQNis8+Z/WluIy+2Wh7KVnHMC/GXudOVINmjhgOT3pCrvqLjUjutI6tAYx3s7WVQVQ70jsjx0fhYvrvBqUxH+mvcPFEpqwyI3uYvOytEy/UDHWfqrlUCHEvIvVmTIAlLVaNlIb22nHKdbnayiAy9SikNzt+NoKtIA2Hh7quzOTcCha+WbD0m4XLNSTqkaXgr359b4BJGwHIAzFzoesk/qpghdkZvTws4TJ1WUZHOAesv9Kmd+L19cydzr86zq7jEfkszZHEoRk75YkNSg4XKV6tYx0oG9sSShXzrWknFh8I0+tabf2DeMv22Qv8fhIzxw38yVZY3IWDuJjjePzyZoRa02ysjCMiin8dSZlFdtJ+LZiaSXPYp5zx80Glgq6bqhzgN5omrVDwBf8MZki+blMjvGB5kwAm+xt32eARq9jsYaBA+ipFVm38NfV6aZfQGFJZdxdVPWgVluUs1whdHbUWHXT17C13lXk8BKQzIXWL3P9uG+8Z4+ljoTwZAAfDN8y6hepaxu69JBfMJhYrsCl4Gqo6qGeAttSCkDqyHFLH/H/iFU+tajTBWyV7JjE7Yo8VI3gAAAAAAAAAAAAAAAAAAAAAAADA9MCEwCQYFKw4DAhoFAAQUtwK8OvOYFJU8i2EeerHni+aMbH8EFCUiqy6mRvG2A7qgMRlk2/y/DTqoAgIEAAAA",
        "GrupaKlientow": "opole",
        "AdresBazowyRestApi": "https://uonetplus-komunikacja.eszkola.opolskie.pl/opole/",
        "UzytkownikLogin": "<wycięto (adres e-mail)>",
        "UzytkownikNazwa": "<wycięto (adres e-mail)>",
        "TypKonta": null
    }
}
```

Następnie należy wysłać żądanie `POST` na adres:

```
${adres REST API}/${symbol}/mobile-api/Uczen.v3.UczenStart/ListaUczniow
```

 Żądanie musi posiadać następujące nagłówki:

```
RequestSignatureValue: <podpis żądania>
User-Agent: MobileUserAgent
RequestCertificateKey: <"CertyfikatKlucz" z poprzedniej odpowiedzi>
Content-Type: application/json; charset=UTF-8
```

I następującą treść:

```json
{
    "RemoteMobileTimeKey": ${Unix time now},
    "TimeKey": ${Unix time now - 1},
    "RequestId": "${random UUID v4}",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

Przykładowa odpowiedź:

```json
{
    "Status": "Ok",
    "TimeKey": 1524660282,
    "TimeValue": "2018.04.25 14:44:42",
    "RequestId": "",
    "DayOfWeek": 3,
    "AppVersion": "17.09.0009.26859",
    "Data": [
        {
            "IdOkresKlasyfikacyjny": 602,
            "OkresPoziom": 3,
            "OkresNumer": 2,
            "OkresDataOd": 1517439600,
            "OkresDataDo": 1535666400,
            "OkresDataOdTekst": "2018-02-01",
            "OkresDataDoTekst": "2018-08-31",
            "IdJednostkaSprawozdawcza": 6,
            "JednostkaSprawozdawczaSkrot": "OpolePSP2137",
            "JednostkaSprawozdawczaNazwa": "Publiczna Szkoła Podstawowa nr 2137",
            "JednostkaSprawozdawczaSymbol": "OpoleG2137",
            "IdJednostka": 0,
            "JednostkaNazwa": "Publiczne Gimnazjum nr 2137 w Opolu",
            "JednostkaSkrot": "OpoleG2137",
            "OddzialSymbol": "D",
            "OddzialKod": "3D",
            "UzytkownikRola": "uczeń",
            "UzytkownikLogin": "<wycięto (adres e-mail)>",
            "UzytkownikLoginId": 8736,
            "UzytkownikNazwa": "<wycięto (nazwisko)> <wycięto (imię)>",
            "Id": 2560,
            "IdOddzial": 97,
            "Imie": "<wycięto (imię)>",
            "Imie2": "<wycięto (drugie imię)>",
            "Nazwisko": "<wycięto (nazwisko)>",
            "Pseudonim": "",
            "UczenPlec": 1,
            "Pozycja": 0,
            "LoginId": null
        }
    ]
```

Następnie należy wysłać żądanie `POST` na adres:

```
${adres REST API}/${symbol}/${"JednostkaSprawozdawczaSymbol" z poprzedniej odpowiedzi}/mobile-api/Uczen.v3.Uczen/LogAppStart
```

Musi ono posiadać następujące nagłówki:

```
RequestSignatureValue: <podpis żądania>
User-Agent: MobileUserAgent
RequestCertificateKey: <"CertyfikatKlucz">
Content-Type: application/json; charset=UTF-8
```

I następującą treść:

```json
{
    "RemoteMobileTimeKey": ${Unix time now},
    "TimeKey": ${Unix time now - 1},
    "RequestId": "${random UUID v4}",
    "RemoteMobileAppVersion": "18.4.1.388",
    "RemoteMobileAppName": "VULCAN-Android-ModulUcznia"
}
```

Przykładowa odpowiedź:

```json
{
    "Status": "Ok",
    "TimeKey": 1524660294,
    "TimeValue": "2018.04.25 14:44:54",
    "RequestId": "4bd41f78-b783-4cf6-8c28-b21778f93933",
    "DayOfWeek": 3,
    "AppVersion": "17.09.0009.26859",
    "Data": "Log"
}
```

Dalej analizowana aplikacja przystępuje do zwykłego przeglądania zasobów dziennika.